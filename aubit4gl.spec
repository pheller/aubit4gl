Name:		aubit4gl
Version:	1.2.40
Release:	1%{?dist}
Summary:	aubit4gl

License:	GPLv2+
URL:		https://www.aubit.com
Source0:	https://www.aubit.com/aubit4gl/src/aubit4glsrc.%{version}.tar.gz

BuildRequires:	flex bison ncurses-devel postgresql-devel glib2 glib2-devel
Requires:	ncurses-libs postgresql-libs

%description

%package libs
Summary:	libs
Provides: 	libaubit4gl.so()(64bit)

%description libs

%prep
%setup -q -n aubit4glsrc

%build
%configure --prefix=%{buildroot}/usr/local/aubit4gl --disable-prefix-check
make

%install
make install
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
mkdir -p %{buildroot}%{_sysconfdir}/profile.d
/bin/echo /usr/local/aubit4gl/lib > %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf
/bin/echo "export AUBITDIR=/usr/local/aubit4gl" > %{buildroot}%{_sysconfdir}/profile.d/%{name}-%{_arch}.sh
/bin/echo "export PATH=\$PATH:/usr/local/aubit4gl/bin" >> %{buildroot}%{_sysconfdir}/profile.d/%{name}-%{_arch}.sh
/bin/touch %{buildroot}/usr/local/aubit4gl/etc/aubitrc

%files libs
%config %{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf
%config %{_sysconfdir}/profile.d/%{name}-%{_arch}.sh
%config /usr/local/aubit4gl/etc/aubitrc
/usr/local/aubit4gl/lib/libaubit4gl-1.2_40.so
/usr/local/aubit4gl/lib/libaubit4gl.so
/usr/local/aubit4gl/plugins-1.2_40/libUI_HL_TUI.so
/usr/local/aubit4gl/plugins-1.2_40/libUI_HL_TUIN.so
/usr/local/aubit4gl/plugins-1.2_40/libUI_TUI.so
/usr/local/aubit4gl/plugins-1.2_40/libUI_TUI_wide.so
/usr/local/aubit4gl/plugins-1.2_40/libESQL_POSTGRES.so
/usr/local/aubit4gl/plugins-1.2_40/libSQL_pg.so
/usr/local/aubit4gl/plugins-1.2_40/libSQL_pg8.so
/usr/local/aubit4gl/plugins-1.2_40/libA4GL_HTML.so
/usr/local/aubit4gl/plugins-1.2_40/libA4GL_file.so
/usr/local/aubit4gl/plugins-1.2_40/libA4GL_memcache.so
/usr/local/aubit4gl/plugins-1.2_40/libA4GL_pcre.so
/usr/local/aubit4gl/plugins-1.2_40/libA4GL_pick.so
/usr/local/aubit4gl/plugins-1.2_40/libA4GL_roman.so
/usr/local/aubit4gl/plugins-1.2_40/libA4GL_string.so
/usr/local/aubit4gl/plugins-1.2_40/libDATA_module.so
/usr/local/aubit4gl/plugins-1.2_40/libDATA_module_definition.so
/usr/local/aubit4gl/plugins-1.2_40/libDATA_report.so
/usr/local/aubit4gl/plugins-1.2_40/libDATA_struct_form.so
/usr/local/aubit4gl/plugins-1.2_40/libEXREPORT_NOPDF.so
/usr/local/aubit4gl/plugins-1.2_40/libFORM_GENERIC.so
/usr/local/aubit4gl/plugins-1.2_40/libFORM_NOFORM.so
/usr/local/aubit4gl/plugins-1.2_40/libFORM_XDR.so
/usr/local/aubit4gl/plugins-1.2_40/libHELP_std.so
/usr/local/aubit4gl/plugins-1.2_40/libLEX_C.so
/usr/local/aubit4gl/plugins-1.2_40/libLEX_CS.so
/usr/local/aubit4gl/plugins-1.2_40/libLEX_EC.so
/usr/local/aubit4gl/plugins-1.2_40/libLEX_WRITE.so
/usr/local/aubit4gl/plugins-1.2_40/libLOGREPPROC_CSV.so
/usr/local/aubit4gl/plugins-1.2_40/libLOGREPPROC_HTML.so
/usr/local/aubit4gl/plugins-1.2_40/libLOGREPPROC_TXT.so
/usr/local/aubit4gl/plugins-1.2_40/libMSG_NATIVE.so
/usr/local/aubit4gl/plugins-1.2_40/libPACKER_FORMXML.so
/usr/local/aubit4gl/plugins-1.2_40/libPACKER_MEMPACKED.so
/usr/local/aubit4gl/plugins-1.2_40/libPACKER_PACKED.so
/usr/local/aubit4gl/plugins-1.2_40/libPACKER_PERL.so
/usr/local/aubit4gl/plugins-1.2_40/libPACKER_XDR.so
/usr/local/aubit4gl/plugins-1.2_40/libPACKER_XML.so
/usr/local/aubit4gl/plugins-1.2_40/libRPC_NORPC.so
/usr/local/aubit4gl/plugins-1.2_40/libRPC_XDR.so
/usr/local/aubit4gl/plugins-1.2_40/libSQLPARSE_INFORMIX.so
/usr/local/aubit4gl/plugins-1.2_40/libSQLPARSE_NONE.so
/usr/local/aubit4gl/plugins-1.2_40/libSQL_FILESCHEMA.so
/usr/local/aubit4gl/plugins-1.2_40/libSQL_nosql.so
/usr/local/aubit4gl/plugins-1.2_40/libUI_CONSOLE.so
/usr/local/aubit4gl/plugins-1.2_40/libUI_XML.so
/usr/local/aubit4gl/plugins-1.2_40/libXDRPACKER_module.so
/usr/local/aubit4gl/plugins-1.2_40/libXDRPACKER_module_definition.so
/usr/local/aubit4gl/plugins-1.2_40/libXDRPACKER_report.so
/usr/local/aubit4gl/plugins-1.2_40/libXDRPACKER_struct_form.so
/usr/local/aubit4gl/plugins-1.2_40/libbarcode.so
/usr/local/aubit4gl/plugins-1.2_40/libchannel.so
/usr/local/aubit4gl/plugins-1.2_40/liberrhook_sample.so

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
/usr/local/aubit4gl/Makefile
/usr/local/aubit4gl/README.txt
/usr/local/aubit4gl/bin/4GL_metrics.cgi
/usr/local/aubit4gl/bin/4glc
/usr/local/aubit4gl/bin/4glpc
/usr/local/aubit4gl/bin/a4gl
/usr/local/aubit4gl/bin/a4gl.4ae
/usr/local/aubit4gl/bin/asql_p.4ae
/usr/local/aubit4gl/bin/aace
/usr/local/aubit4gl/bin/aace_4gl
/usr/local/aubit4gl/bin/aace_perl
/usr/local/aubit4gl/bin/aace_runner
/usr/local/aubit4gl/bin/adbaccess
/usr/local/aubit4gl/bin/adbschema
/usr/local/aubit4gl/bin/adecompile
/usr/local/aubit4gl/bin/afinderr
/usr/local/aubit4gl/bin/amake
/usr/local/aubit4gl/bin/amkmessage
/usr/local/aubit4gl/bin/aperform
/usr/local/aubit4gl/bin/asql_g.4ae
/usr/local/aubit4gl/bin/aubit
/usr/local/aubit4gl/bin/aubit-config
/usr/local/aubit4gl/bin/configurator
/usr/local/aubit4gl/bin/convertsql
/usr/local/aubit4gl/bin/default_frm
/usr/local/aubit4gl/bin/ecpg_wrap
/usr/local/aubit4gl/bin/fcompile
/usr/local/aubit4gl/bin/fdecompile
/usr/local/aubit4gl/bin/fglproto
/usr/local/aubit4gl/bin/fshow
/usr/local/aubit4gl/bin/generate_aace
/usr/local/aubit4gl/bin/genmake
/usr/local/aubit4gl/bin/ide1.4ae
/usr/local/aubit4gl/bin/loadmap
/usr/local/aubit4gl/bin/mcompile
/usr/local/aubit4gl/bin/mdecompile
/usr/local/aubit4gl/bin/prepmake
/usr/local/aubit4gl/bin/process_report
/usr/local/aubit4gl/bin/quick_check_logrep
/usr/local/aubit4gl/bin/quickguide.4ae
/usr/local/aubit4gl/bin/report.pm
/usr/local/aubit4gl/bin/runner_fgl_wrapper
/usr/local/aubit4gl/bin/shtool
/usr/local/aubit4gl/bin/sql_parse
/usr/local/aubit4gl/bin/unmkmessage
/usr/local/aubit4gl/bin/using.pm
/usr/local/aubit4gl/configure
/usr/local/aubit4gl/docs/COPYING
/usr/local/aubit4gl/docs/CREDITS
/usr/local/aubit4gl/docs/LICENSE
/usr/local/aubit4gl/docs/aubit4gl.ico
/usr/local/aubit4gl/docs/aubit4gl.png
/usr/local/aubit4gl/etc/4glpc.hlp
/usr/local/aubit4gl/etc/Aubit4GL.jpg
/usr/local/aubit4gl/etc/aubitrc-bin.in
/usr/local/aubit4gl/etc/change.afr.dat
/usr/local/aubit4gl/etc/config/.cvsignore
/usr/local/aubit4gl/etc/config/bootstrap
/usr/local/aubit4gl/etc/config/config.guess
/usr/local/aubit4gl/etc/config/config.sub
/usr/local/aubit4gl/etc/config/install-sh
/usr/local/aubit4gl/etc/config/ltmain.sh
/usr/local/aubit4gl/etc/config/missing
/usr/local/aubit4gl/etc/config/mkinstalldirs
/usr/local/aubit4gl/etc/convertsql/INFORMIX-DB2.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-DB2VM.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-INFORMIX.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-INGRES.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-MYSQL.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-MYSQLDIRECT.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-ORACLE.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-POSTGRES.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-POSTGRES8.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-POSTGRESQL.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-SQLITE.cnv
/usr/local/aubit4gl/etc/convertsql/INFORMIX-SQLSERVER.cnv
/usr/local/aubit4gl/etc/convertsql/ORACLE-INFORMIX.cnv
/usr/local/aubit4gl/etc/convertsql/POSTGRESQL-INFORMIX.cnv
/usr/local/aubit4gl/etc/convertsql/README.txt
/usr/local/aubit4gl/etc/gtkrc_2
/usr/local/aubit4gl/etc/help_aubit.hlp
/usr/local/aubit4gl/etc/helpfile.hlp
/usr/local/aubit4gl/etc/helpsql_POSTGRES.hlp
/usr/local/aubit4gl/etc/helpsql_POSTGRES8.hlp
/usr/local/aubit4gl/etc/helpsql_SQLITE.hlp
/usr/local/aubit4gl/etc/import/README
/usr/local/aubit4gl/etc/import/a4gl_file
/usr/local/aubit4gl/etc/import/a4gl_glade
/usr/local/aubit4gl/etc/import/a4gl_html
/usr/local/aubit4gl/etc/import/a4gl_pcre
/usr/local/aubit4gl/etc/import/a4gl_string
/usr/local/aubit4gl/etc/import/channel
/usr/local/aubit4gl/etc/import/default
/usr/local/aubit4gl/etc/odbc.ini.example
/usr/local/aubit4gl/etc/options.afr.dat
/usr/local/aubit4gl/etc/values.afr.dat
/usr/local/aubit4gl/incl/Makefile-common
/usr/local/aubit4gl/incl/Makefile-common.in
/usr/local/aubit4gl/incl/Makefile-install.mki
/usr/local/aubit4gl/incl/a4gl.mk
/usr/local/aubit4gl/incl/a4gl_4gl_callable.h
/usr/local/aubit4gl/incl/a4gl_API_esql.h
/usr/local/aubit4gl/incl/a4gl_API_exreport.h
/usr/local/aubit4gl/incl/a4gl_API_form.h
/usr/local/aubit4gl/incl/a4gl_API_help.h
/usr/local/aubit4gl/incl/a4gl_API_menu.h
/usr/local/aubit4gl/incl/a4gl_API_rpc.h
/usr/local/aubit4gl/incl/a4gl_API_sql.h
/usr/local/aubit4gl/incl/a4gl_API_sqlparse.h
/usr/local/aubit4gl/incl/a4gl_API_ui.h
/usr/local/aubit4gl/incl/a4gl_builtin_funcs.h
/usr/local/aubit4gl/incl/a4gl_esql.h
/usr/local/aubit4gl/incl/a4gl_esql_infoflex.h
/usr/local/aubit4gl/incl/a4gl_esql_infx.h
/usr/local/aubit4gl/incl/a4gl_esql_ingres.h
/usr/local/aubit4gl/incl/a4gl_esql_postgres.h
/usr/local/aubit4gl/incl/a4gl_exdata.h
/usr/local/aubit4gl/incl/a4gl_expr.h
/usr/local/aubit4gl/incl/a4gl_incl_4gldef.h
/usr/local/aubit4gl/incl/a4gl_incl_4glhdr.h
/usr/local/aubit4gl/incl/a4gl_incl_config.h
/usr/local/aubit4gl/incl/a4gl_incl_infx.h
/usr/local/aubit4gl/incl/a4gl_memhandling.h
/usr/local/aubit4gl/incl/a4gl_rep_structure.h
/usr/local/aubit4gl/incl/a4gl_screenio.h
/usr/local/aubit4gl/incl/d4gl.mk
/usr/local/aubit4gl/incl/dataio/fgl.xs.h
/usr/local/aubit4gl/incl/dataio/form_x.xs.h
/usr/local/aubit4gl/incl/dataio/npcode.xs.h
/usr/local/aubit4gl/incl/dataio/report.xs.h
/usr/local/aubit4gl/incl/footer-c.mki
/usr/local/aubit4gl/incl/footer.mki
/usr/local/aubit4gl/incl/footer2.mki
/usr/local/aubit4gl/incl/header.mki
/usr/local/aubit4gl/incl/i4gl.mk
/usr/local/aubit4gl/incl/q4gl.mk
/usr/local/aubit4gl/install.sh
/usr/local/aubit4gl/tools/4glpc/settings/C
/usr/local/aubit4gl/tools/4glpc/settings/C_INFORMIX
/usr/local/aubit4gl/tools/4glpc/settings/EC
/usr/local/aubit4gl/tools/4glpc/settings/EC_INFOFLEX
/usr/local/aubit4gl/tools/4glpc/settings/EC_INFORMIX
/usr/local/aubit4gl/tools/4glpc/settings/EC_INGRES
/usr/local/aubit4gl/tools/4glpc/settings/EC_POSTGRES
/usr/local/aubit4gl/tools/4glpc/settings/EC_SAP
/usr/local/aubit4gl/tools/4glpc/settings/INFORMIX
/usr/local/aubit4gl/tools/4glpc/settings/PCODEC
/usr/local/aubit4gl/tools/4glpc/settings/SPL_INFORMIX
/usr/local/aubit4gl/tools/4glpc/settings/aix
/usr/local/aubit4gl/tools/4glpc/settings/aix__EC_INFORMIX
/usr/local/aubit4gl/tools/4glpc/settings/darwin
/usr/local/aubit4gl/tools/4glpc/settings/darwin__EC_POSTGRES
/usr/local/aubit4gl/tools/4glpc/settings/i686-pc-linux-gnuaout-cc__C
/usr/local/aubit4gl/tools/4glpc/settings/i686-pc-linux-gnuaout-cc__EC
/usr/local/aubit4gl/tools/4glpc/settings/mingw__C
/usr/local/aubit4gl/tools/4glpc/settings/mingw__EC_INFORMIX
/usr/local/aubit4gl/tools/4glpc/settings/mingw__EC_POSTGRES
/usr/local/aubit4gl/tools/4glpc/settings/x86_64-unknown-linux-gnu__C
/usr/local/aubit4gl/tools/4glpc/settings/x86_64-unknown-linux-gnu__EC
/usr/local/aubit4gl/tools/4glpc/settings/x86_64-unknown-linux-gnu__EC_INFORMIX
/usr/local/aubit4gl/tools/4glpc/settings/x86_64-unknown-linux-gnu__EC_POSTGRES
/usr/local/aubit4gl/tools/test/Makefile
/usr/local/aubit4gl/tools/test/assoc2.4gl
/usr/local/aubit4gl/tools/test/file.4gl
/usr/local/aubit4gl/tools/test/form.per
/usr/local/aubit4gl/tools/test/gui/Makefile
/usr/local/aubit4gl/tools/test/gui/calc.4gl
/usr/local/aubit4gl/tools/test/gui/calc.per
/usr/local/aubit4gl/tools/test/gui/form-gui.per
/usr/local/aubit4gl/tools/test/gui/gui.4gl
/usr/local/aubit4gl/tools/test/gui/gui.per
/usr/local/aubit4gl/tools/test/gui/hello-gui.4gl
/usr/local/aubit4gl/tools/test/gui/multi.per
/usr/local/aubit4gl/tools/test/gui/radio.per
/usr/local/aubit4gl/tools/test/gui/widget.per
/usr/local/aubit4gl/tools/test/hello.4gl
/usr/local/aubit4gl/tools/test/hello2.4gl
/usr/local/aubit4gl/tools/test/hello_db.4gl
/usr/local/aubit4gl/tools/test/helpfile.msg
/usr/local/aubit4gl/tools/test/pdf_report.4gl
/usr/local/aubit4gl/tools/test/test_build.4gl
/usr/local/aubit4gl/tools/test/test_select.4gl
/usr/local/aubit4gl/tools/test/testmenu.4gl
/usr/local/aubit4gl/tools/test/testmenu.menu
/usr/local/aubit4gl/tools/test/testmpz.4gl

%doc

%changelog

